
import socket


class webApp:

    def parse(self, received):
        return None

    def process(self, analyzed):
        http = "200 OK"
        html = "<html><body><h1>Hello World!</h1></body></html>"
        return (http, html)

    def __init__(self, host, port):
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((host, port))
        mySocket.listen(5)

        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            print("HTTP request received:")
            received = recvSocket.recv(2048)
            print(received)

            # Parseo la petición y me quedo con lo que me interesa
            petition = self.parse(received)

            # Proceso la petición y ejecuto el código del servidor
            http, html = self.process(petition)

            # Creo la respuesta al cliente y la envío
            response = "HTTP/1.1" + http + "\r\n\r\n" \
                       + html + "\r\n"

            recvSocket.send(response.encode('utf-8'))
            recvSocket.close()


if __name__ == "__main__":
    testWebApp = webApp('localhost', 1234)


