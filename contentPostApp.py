# EJERCICIO 19.4. Clase contentPostApp


import webapp

formulario = """
    <form action= " " method="POST"> 
    <input type="text" name="content">
    <input type="submit" value="Enviar"></form>
"""


class contentPostApp(webapp.webApp):

    recursos = {'/': "P&aacute;gina principal", 'holamundo': "Hola mundo!", 'adiosmundo': "Adi&oacute;s mundo!"}

    def parse(self, received):
        method = received.decode().split(' ')[0]  # método
        resource = received.decode().split(' ')[1][1:]  # recurso
        # Divido por \r\n\r\n
        if method == "POST":
            body = received.decode().split('\r\n\r\n')[1]  # cuerpo
        else:
            body = None

        # Devuelve un valor que es una tupla
        return method, resource, body

    def process(self, analyzed):
        method, resource, body = analyzed

        if method == "POST":
            self.recursos[resource] = body
            http = "200 OK"
            html = "<html><body><h4>Introducir contenido</h4>Recurso solicitado: " + resource + \
                   "<br>Contenido: " + self.recursos[resource] + formulario + "</body></html>"
        else:
            if resource in self.recursos:
                http = "200 OK"
                html = "<html><body><h4>P&aacute;gina encontrada</h4>Recurso solicitado: " + resource + "<br>" \
                       + "Contenido: " + self.recursos[resource] + \
                       "<p>Introducir nuevos contenidos:</p>" + formulario + "</body></html>"
            else:
                http = "404 Not Found"
                html = "<html><body><h4>P&aacute;gina no encontrada</h4><p>Puedes introducir contenido:</p>" + formulario + "</body></html>"
            return http, html
        return http, html


if __name__ == "__main__":
    contentpostapp = contentPostApp('localhost', 1234)

